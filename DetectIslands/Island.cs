using System;
using System.Collections.Generic;

namespace DetectIslands
{
	struct Coord
	{
		public int x;
		public int y;

		public Coord( int x, int y )
		{
			this.x = x;
			this.y = y;
		}

		public int AsIndex( int width )
		{
			return y*width+x;
		}
	}

	class Island
	{
		UInt32[] mCells;
		int mWidth;
		int mHeight;
		bool mProcessed;

		public Island( UInt32[] cells, int width )
		{
			mCells = cells;
			mWidth = width;
			mHeight = cells.Length / width;
			mProcessed = false;
		}

		public Island( string[] strs )
		{
			mWidth = strs[0].Length;
			mHeight = strs.Length;
			mCells = new UInt32[mWidth*mHeight];
			int index=0;
			for(int y=0;y<mHeight;++y)
			{
				for(int x=0;x<mWidth;++x)
				{
					mCells[index++] = (UInt32)(strs[y][x]-'0');
				}
			}
			mProcessed = false;
		}

		public void Display()
		{
			int x = 0;
			int index = 0;
			while( index < mCells.Length )
			{
				if( mCells[index] == 0 )
				{
					Console.Write( '-' );
				}
				else
				{
					Console.Write( mCells[index] );
				}
				if( ++x >= mWidth )
				{
					x=0;
					Console.Write("\n");
				}
				++index;
			}

		}

		public void Detect()
		{
			if( mProcessed )
			{
				return;
			}
			mProcessed = true;

			// NJM - Change the 1's to maxValue
			for( int i=0;i<mCells.Length;++i)
			{
				mCells[i] *= UInt32.MaxValue; // NJM - avoid 'if' to maintain pipeline
			}

			int index=0;
			uint islandCount = 0;
			for( int y=0; y<mHeight; ++y )
			{
				for( int x=0; x<mWidth; ++x )
				{
					// is this an unprocessed island?
					if( mCells[index] == UInt32.MaxValue )
					{
						Console.WriteLine(string.Format("Found {2}th island at ({0},{1})",x, y, islandCount+1));
						WalkIsland( x, y, ++islandCount );
					}
					index++;
				}
			}
		}

		private void WalkIsland( int x, int y, uint islandCount )
		{
			List<Coord> walkPoints = new List<Coord>();
			Coord startCoord = new Coord( x, y );
			walkPoints.Add( startCoord );
			mCells[startCoord.AsIndex(mWidth)] = islandCount;

			while( walkPoints.Count > 0 )
			{
				Coord nextWP = walkPoints[0];
				walkPoints.RemoveAt(0);
				if( nextWP.x > 0 )
				{
					nextWP.x--;
					int i = nextWP.AsIndex(mWidth);
					if( mCells[i] == UInt32.MaxValue )
					{
						mCells[i] = islandCount;
						walkPoints.Add( nextWP );
					}
					nextWP.x++;
				}
				if( nextWP.x < mWidth-1 )
				{
					nextWP.x++;
					int i = nextWP.AsIndex(mWidth);
					if( mCells[i] == UInt32.MaxValue )
					{
						mCells[i] = islandCount;
						walkPoints.Add( nextWP );
					}
					nextWP.x--;
				}
				if( nextWP.y > 0 )
				{
					nextWP.y--;
					int i = nextWP.AsIndex(mWidth);
					if( mCells[i] == UInt32.MaxValue )
					{
						mCells[i] = islandCount;
						walkPoints.Add( nextWP );
					}
					nextWP.y++;
				}
				if( nextWP.y < mHeight-1 )
				{
					nextWP.y++;
					int i = nextWP.AsIndex(mWidth);
					if( mCells[i] == UInt32.MaxValue )
					{
						mCells[i] = islandCount;
						walkPoints.Add( nextWP );
					}
					nextWP.y--;
				}
			}
		}

		public UInt32[] GetRaw()
		{
			return mCells;
		}

		public UInt32 GetCell(int index)
		{
			return mCells[index];
		}
	}

}
