using System;
using System.Collections.Generic;

namespace DetectIslands
{
	class Test
	{
		public static int Run ()
		{
			Console.WriteLine ("running tests...");
			bool success = true;

			Island island1 = new Island( new UInt32[] { 1, 0, 0, 1 }, 2 );
			Island island2 = new Island( new UInt32[] { 1, 0, 0, 2 }, 2 );
			island1.Detect();
			success &= CompareIslands( island1, island2 );


			island1 = new Island( new UInt32[] {
				1, 0, 0, 1,
				1, 0, 0, 1, 
				0, 1, 1, 0,
				0, 1, 1, 0 }, 4 );
			island2 = new Island( new UInt32[] {
				1, 0, 0, 2,
				1, 0, 0, 2, 
				0, 3, 3, 0,
				0, 3, 3, 0 }, 4 );
			island1.Detect();
			success &= CompareIslands( island1, island2 );

			string[] strs = new string[] {
				"000000000000000000", 
				"011100000000000000",
				"000110000000000000", 
				"001110000000000000", 
				"000000001010001110", 
				"000010011110001100", 
				"000011111110011100",
				"000011111011100000", 
				"000000001110000000"
			};
			island1 = new Island( strs );
			strs = new string[] {
				"000000000000000000", 
				"011100000000000000",
				"000110000000000000", 
				"001110000000000000", 
				"000000002020003330", 
				"000020022220003300", 
				"000022222220033300",
				"000022222022200000", 
				"000000002220000000"
			};
			island2 = new Island( strs );
			island1.Detect();
			success &= CompareIslands( island1, island2 );

			strs = new string[] {
				"111111111000000000000000" ,
				"111111111110000010000000",
				"111100000110000011000000" ,
				"111000000111100000000000" ,
				"110001000111110000000000" ,
				"100000100111110000000100" ,
				"000001110111111000001110" ,
				"100011111110111000000110" ,
				"100111100000011001000000" ,
				"100111100100011011000000" ,
				"100111101110011110000000" ,
				"100111001100010110000000" ,
				"100111100001110111100000" ,
				"000111111111110001100000" ,
				"000000000111110000111000" ,
				"000000000011110000001000" ,
				"000000000001110000001100" ,
				"000000000001110000000000",
			};
			island1 = new Island( strs );
			strs = new string[] {
				"111111111000000000000000" ,
				"111111111110000020000000",
				"111100000110000022000000" ,
				"111000000111100000000000" ,
				"110003000111110000000000" ,
				"100000100111110000000400" ,
				"000001110111111000004440" ,
				"500011111110111000000440" ,
				"500111100000011001000000" ,
				"500111100600011011000000" ,
				"500111106660011110000000" ,
				"500111006600010110000000" ,
				"500111100001110111100000" ,
				"000111111111110001100000" ,
				"000000000111110000111000" ,
				"000000000011110000001000" ,
				"000000000001110000001100" ,
				"000000000001110000000000",
			};
			island2 = new Island( strs );
			island1.Detect();
			success &= CompareIslands( island1, island2 );

			return success?1:-1;
		}

		private static void OutputUsage()
		{
			Console.WriteLine ("USAGE: DetectIslands [island.txt]");
		}

		private static bool CompareIslands( Island i1, Island i2 )
		{
			bool match = false;
			if( i1.GetRaw().Length == i2.GetRaw().Length )
			{
				match = true;
				for(int i=0;i<i1.GetRaw().Length;++i)
				{
					if( i1.GetCell(i) != i2.GetCell(i) )
					{
						Console.WriteLine( "islands differ, island 1..." );
						i1.Display();
						Console.WriteLine( "island 2..." );
						i2.Display();
						match = false;
						break;
					}
				}
			}
			return match;
		}
	}
}
