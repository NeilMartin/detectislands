using System;

namespace DetectIslands
{
	class MainClass
	{
		public static int Main (string[] args)
		{
			if( args.Length == 0 )
			{
				OutputUsage();
				Test.Run();
				return 1;
			}
			else if ( args.Length == 1 )
			{
				string[] lines = System.IO.File.ReadAllLines(args[0]);
				Island island = new Island( lines );
				island.Detect();
				island.Display();
				return 1;
			}
			else
			{
				OutputUsage();
				return -1;
			}
		}

		private static void OutputUsage()
		{
			Console.WriteLine ("USAGE: DetectIslands [island.txt]");
		}
	}
}
