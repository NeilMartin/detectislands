Neil Martin, test for GREE. 4th March 2015

Both the space and time requirements for the algorithm I
have used is O(n). Where n is the number of cells that
represent the island/ocean.

Island.Detect performs the terrain analysis by walking over
the cells in search for an undiscovered island (uint32.max)
When an undiscovered island is found, it will paint the
island with the island index by walking over it with
Island.WalkIsland.

The islands are stored in a uint32 array within Island.
The WalkIsland function requires a List of walk points,
this list can grow to 2xwidth of an island.

Example output...

C:\..\DetectIslands\DetectIslands\bin\Debug>DetectIslands.exe Island2.txt
Found 1th island at (0,0)
Found 2th island at (16,1)
Found 3th island at (5,4)
Found 4th island at (21,5)
Found 5th island at (0,7)
Found 6th island at (9,9)
111111111---------------
11111111111-----2-------
1111-----11-----22------
111------1111-----------
11---3---11111----------
1-----1--11111-------4--
-----111-111111-----444-
5---1111111-111------44-
5--1111------11--1------
5--1111--6---11-11------
5--1111-666--1111-------
5--111--66---1-11-------
5--1111----111-1111-----
---11111111111---11-----
---------11111----111---
----------1111------1---
-----------111------11--
-----------111----------